import matplotlib.pyplot as plt
import pandas as pd

data = pd.read_csv('data/results.csv', index_col='n')

data.plot()
plt.show()
