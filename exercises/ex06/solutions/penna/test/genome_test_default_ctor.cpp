#include <genome.hpp>

#include <cstdlib> // for std::srand
#include <stdexcept> // for std::logic_error

using namespace Penna;

void test_default_constructor() {
    Genome genome;

    if (genome.count_bad(Genome::number_of_genes - 1) != 0)
      throw std::logic_error("Default initialized Genome is not all zero.");
}

int main() {
    // seed the random number generator
    std::srand(42);

    // run tests
    test_default_constructor();
}
