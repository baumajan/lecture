cmake_minimum_required(VERSION 3.1)
project(benchmark)

set(CMAKE_CXX_STANDARD 11)

# If the library is installed in a standard location, we would use find_library
# and not specify this manually.
# We choose to do it manually to keep it simpler.
if(NOT LIBRARY_INSTALL_PATH)  # CMake was not invoced with -DLIBRARY_INSTALL_PATH.
    set(LIBRARY_INSTALL_PATH ${CMAKE_SOURCE_DIR}/../install)  # hardcoded
endif()

link_directories(${LIBRARY_INSTALL_PATH}/lib)
include_directories(${LIBRARY_INSTALL_PATH}/include)

add_executable(benchmark main.cpp)
target_link_libraries(benchmark random timer)

install(TARGETS benchmark DESTINATION bin)
