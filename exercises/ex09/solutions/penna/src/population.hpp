/**
 * Header for the Penna population class.
 * Programming Techniques for Scientific Simulations, ETH Zürich
 */

#ifndef POPULATION_HPP
#define POPULATION_HPP

#include "animal.hpp"
#include "penna_vector.hpp"
#include <list>
#include <random>

namespace Penna {
/**
 * Population of animals.
 */
class Population {
    #ifdef PENNA_VECTOR
    using container_t = penna_vector<Animal>;
    #else
    using container_t = std::list<Animal>;
    #endif

public:
    using const_iterator = container_t::const_iterator;
    /**
     * Constructor.
     * @param nmax Maximum population size. Parameter N_{max} in Penna's paper.
     * @param n0 Initial population size.
     */
    Population( const size_t & nmax, const size_t & n0 );

    /// Classes with a vtable should have a virtual destructor.
    virtual ~Population();

    /// Simulate growth of the population for time years.
    void simulate( size_t time );

    /// Simulate one time step (year).
    virtual void step();

    /// Get size of population.
    std::size_t size() const;

    /// Get iterators for the population_ container.
    const_iterator begin() const;
    const_iterator end() const;

protected:
    container_t population_;

private:
    std::size_t nmax_;
};

} // end namespace Penna

#endif // !defined POPULATION_HPP
