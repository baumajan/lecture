/*
 * Programming Techniques for Scientific Simulations I
 * HS 2018
 * Week 11
 */

#include <iostream>

// we can't change the factorial, since loops are done with recursions in
// the functional paradigm
template<size_t N>
struct factorial {
    enum{ value =  N * factorial<N-1>::value };
};

template<>
struct factorial<0> {
    enum { value = 1 };
};


// we are using the recursive formula (N, k) = (N-1, k-1) + (N-1, k)
// with (N, 0) = (N, N) = 1
template<size_t N, size_t k>
struct binomial {
    enum{ value = binomial<N-1, k-1>::value + binomial<N-1, k>::value };
};

template<size_t N>
struct binomial<N, 0> {
    enum{ value =  1 };
};

template<size_t N>
struct binomial<N, N> {
    enum{ value =  1 };
};


int main()
{
    std::cout << "Factorial:" << std::endl;
    std::cout << "    5!=" << factorial<5>::value << std::endl;
    std::cout << "    20!=" << factorial<20>::value << std::endl;
    std::cout << "    21!=" << factorial<21>::value << std::endl;
    //std::cout << "    70!=" << factorial<70>::value << std::endl;

    std::cout << "Binomial:" << std::endl;
    std::cout << "    N=20, k=1 ==> " << binomial<20, 1>::value << std::endl;
    std::cout << "    N=21, k=1 ==> " << binomial<21, 1>::value << std::endl;
    std::cout << "    N=70, k=1 ==> " << binomial<70, 1>::value << std::endl;
    return 0;
}
