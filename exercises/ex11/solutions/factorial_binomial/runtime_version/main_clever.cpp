/*
 * Programming Techniques for Scientific Simulations I
 * HS 2018
 * Week 11
 */

#include <iostream>
#include <iomanip>
#include <assert.h>

size_t factorial(const size_t & value)
{
    size_t res = 1;
    for(size_t i = 1; i <= value; ++i) {
        res *= i;
    }

    return res;
}

size_t binomial(const size_t & N, const size_t & k)
{
    size_t res = 1;
    for(size_t i = 1; i <= k; ++i) {
        res *= (N+1-i);
        assert(res % i == 0); // make sure there is no remainder (int-arithmetics)
        res /= i; // I divide after the mult, so we dont need double-arithmetics
    }
    return res;
}


int main()
{
    std::cout << "Factorial:" << std::endl;
    for(size_t N = 0; N < 10; ++N) {
        std::cout << "    " << N << "! = " << factorial(N) << std::endl;
    }

    std::cout << "Binomial:" << std::endl;
    for(size_t N = 1; N < 10; ++N) {
        std::cout << "    " << std::setw(5) << "N=" << N ;
        for(size_t k = 0; k <= N; ++k) {
            std::cout << std::setw(5) << binomial(N, k);
        }
        std::cout << std::endl;
    }

    std::cout << "Limits:" << std::endl;
    std::cout << "    20! = " << factorial(20) << std::endl;
    std::cout << "    21! = " << factorial(21) << std::endl;
    std::cout << "    70! = " << factorial(70) << std::endl;

    std::cout << "Limits:" << std::endl;
    std::cout << "    N=20, k=1 ==> " << binomial(20,1) << std::endl;
    std::cout << "    N=21, k=1 ==> " << binomial(21,1) << " <- right " << std::endl;
    std::cout << "    N=70, k=1 ==> " << binomial(70,1) << std::endl;

    return 0;
}
