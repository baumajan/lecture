/* Programming Techniques for Scientific Simulations I
 * HS 2018
 * Week 11
 */

#ifndef ALGO_DERIVATION
#define ALGO_DERIVATION

enum OP_enum {Add, Multiply};

//********

template<typename T>
class Constant {
public:
    using derivative_type = Constant<T>;

    Constant(const T & v) : val_(v) {}
    T operator()(const T &) const {
        return val_;
    }
    derivative_type derivative() const {
        return derivative_type(0);
    }
private:
    T val_;
};

// this function is just here so we don't need to write <int>. The function
// can deduce it (whereas the class can't)
template <typename T>
Constant<T> constant(const T & x) {
    return Constant<T>(x);
}

//********

template <typename T>
class Variable {
public:
    using derivative_type = Constant<T>;

    T operator()(const T & x) const {
        return x;
    }
    derivative_type derivative() const {
        return derivative_type(1);
    }
};

//********
template<typename L, typename R, OP_enum op>
class Expression;

template<typename L, typename R, OP_enum op>
struct expression_helper;

template<typename L, typename R>
struct expression_helper<L, R, Add> {
    using derivative_type = Expression< typename L::derivative_type
                                      , typename R::derivative_type
                                      , Add
                                      >;
    static derivative_type derivative(const L & l, const R & r) {
        return derivative_type(l.derivative(), r.derivative());
    }
};

template<typename L, typename R>
struct expression_helper<L, R, Multiply> {
    using left_helper = Expression< typename L::derivative_type
                                  , R
                                  , Multiply
                                  >;
    using right_helper = Expression< L
                                   , typename R::derivative_type
                                   , Multiply
                                   >;
    using derivative_type = Expression< left_helper
                                      , right_helper
                                      , Add
                                      >;
    static derivative_type derivative(const L & l, const R & r) {
        return derivative_type(left_helper(l.derivative(), r)
                             , right_helper(l, r.derivative())
                             );
    }
};



template<typename L, typename R, OP_enum op>
class Expression {
public:
    using derivative_type = typename expression_helper<L, R, op>::derivative_type;

    Expression(const L & l, const R & r) : l_(l), r_(r) { }

    template <typename T>
    T operator()(const T & x) const {
        switch (op) { // we could just as well use if ... else if .... else
            case Add:
                return l_(x) + r_(x);
            case Multiply:
                return l_(x) * r_(x);
        }
    }

    derivative_type derivative() const {
        return expression_helper<L, R, op>::derivative(l_, r_);
    }

private:
    L l_;
    R r_;
};

//********

template<typename L, typename R>
Expression<L, R, Multiply> operator*(const L & l, const R & r) {
    return Expression<L, R, Multiply>(l, r);
}

template<typename L, typename R>
Expression<L, R, Add> operator+(const L & l, const R & r) {
    return Expression<L, R, Add>(l, r);
}

#endif //ALGO_DERIVATION
