#include <iostream>

class A {
  public:
    virtual void f() const { std::cout << "A::f "; }
    void g() const { std::cout << "A::g "; }
};

class B: public A {
  public:
    void f() const { std::cout << "B::f "; }
    void g() { std::cout << "B::g "; }
};

class C: public B {
  public:
    void f() { std::cout << "C::f "; }
    void g() const { std::cout << "C::g "; }
};

void func(A const& a) {
  a.f();
  a.g();
}

int main() {
  A a;
  B b;
  C c;

  a.f();
  a.g();

  b.f();
  b.g();

  c.f();
  c.g();

  func(a);
  func(b);
  func(c);
}
