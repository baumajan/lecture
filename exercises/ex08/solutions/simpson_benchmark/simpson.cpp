#include "simpson.hpp"
#include "test_functions_macros.hpp"

////////////////////////////////////////////////////////////////////////////////
// Hard-coded version
////////////////////////////////////////////////////////////////////////////////
double simpson_hardcoded(const double a,
                         const double b,
                         const unsigned bins) {
    // std::cout << "Hard-coded version" << std::endl;

    using argument_t = double;
    using result_t = double;

    if (bins == 0)
        throw std::logic_error("Simpson_integrate(..) : Number of bins has to be positive.");

    const argument_t dr = (b - a) / static_cast<argument_t>(2*bins);
    result_t I2(0), I4(0);
    argument_t pos = a;
    
    for (unsigned int i = 0; i < bins; ++i) {
        pos += dr;
        I4 += FUNCTION_TO_INTEGRATE(pos);
        pos += dr;
        I2 += FUNCTION_TO_INTEGRATE(pos);
    }

    return (FUNCTION_TO_INTEGRATE(a) + 2.*I2 + 4.*I4 - FUNCTION_TO_INTEGRATE(b)) * (dr/3.);
}


////////////////////////////////////////////////////////////////////////////////
// Function pointer version
////////////////////////////////////////////////////////////////////////////////
double simpson_function_ptr(double (*func)(double),
                            const double a,
                            const double b,
                            const unsigned bins) {
    // std::cout << "Function pointer version" << std::endl;

    using argument_t = double;
    using result_t = double;

    if (bins == 0)
        throw std::logic_error("Simpson_integrate(..) : Number of bins has to be positive.");

    const argument_t dr = (b - a) / static_cast<argument_t>(2*bins);
    result_t I2(0), I4(0);
    argument_t pos = a;
    
    for (unsigned int i = 0; i < bins; ++i) {
        pos += dr;
        I4 += func(pos);
        pos += dr;
        I2 += func(pos);
    }

    return (func(a) + 2.*I2 + 4.*I4 - func(b)) * (dr/3.);
}


////////////////////////////////////////////////////////////////////////////////
// Abstract base class version (runtime polymorphism)
////////////////////////////////////////////////////////////////////////////////
Function::result_type simpson_base_class(const Function& func,
                                         const Function::argument_type a,
                                         const Function::argument_type b,
                                         const unsigned bins) {                                        
    // std::cout << "Abstract base class version" << std::endl;

    using argument_t = Function::argument_type;
    using result_t = Function::result_type;

    if (bins == 0)
        throw std::logic_error("Simpson_integrate(..) : Number of bins has to be positive.");
    
    const argument_t dr = (b - a) / static_cast<argument_t>(2*bins);
    result_t I2(0), I4(0);
    argument_t pos = a;
    
    for (unsigned int i = 0; i < bins; ++i) {
        pos += dr;
        I4 += func(pos);
        pos += dr;
        I2 += func(pos);
    }

    return (func(a) + 2.*I2 + 4.*I4 - func(b)) * (dr/3.);
}
