#include <iostream>
#include <chrono>
#include <cmath>
#include <stdexcept>
#include <type_traits> // std::enable_if

#include "simpson.hpp"
#include "test_functions_macros.hpp"

// There are better and more elegant solutions than this macro to avoid code duplication.
// They require, however, techniques that go beyond the scope of this course.
#define MEASURE_AND_PRINT_TIME(EXPR)                                                                \
t_sum1 = 0;                                                                                         \
t_sum2 = 0;                                                                                         \
for (unsigned r = 0; r < runs; ++r) {                                                               \
    t_start = std::chrono::high_resolution_clock::now();                                            \
    result = EXPR;                                                                                  \
    t_end = std::chrono::high_resolution_clock::now();                                              \
                                                                                                    \
    if (std::abs(result - exact_result) > tolerance) {                                              \
        std::cerr << "Numerical result deviates from the exact result by "                          \
                  << std::abs(result - exact_result)                                                \
                  << ", which is more than the allowed tolerance " << tolerance                     \
                  << "(exact result = " << exact_result << ", numerical result = " << result        \
                  << ")" << std::endl;                                                              \
                                                                                                    \
        throw std::runtime_error("Numerical result does not agree with exact result.");             \
    }                                                                                               \
                                                                                                    \
    duration = static_cast<std::chrono::duration<double>>(t_end - t_start).count();                 \
    t_sum1 += duration;                                                                             \
    t_sum2 += duration*duration;                                                                    \
}                                                                                                   \
                                                                                                    \
std::cout << t_sum1/runs << " +/- " << std::sqrt((t_sum2 - t_sum1*t_sum1/runs) / (runs-1.))         \
          << std::flush;                                                                            \
// MEASURE_AND_PRINT_TIME


// For templated function object version
struct template_function_object {
    using argument_type = double;
    using result_type = double;

    inline result_type operator()(const argument_type x) const { return FUNCTION_TO_INTEGRATE(x); }
};

// For abstract base class version
struct derived_function_object : public Function {
  result_type operator()(const argument_type x) const { return FUNCTION_TO_INTEGRATE(x); }
};

// For function pointer version
double function_to_integrate(double x) {
    return FUNCTION_TO_INTEGRATE(x);
}


int main() {
    const double a = 0.0;
    const double b = 1.0;
    const unsigned bins = 1024*1024*4;

    // Used in the macro MEASURE_AND_PRINT_TIME.
    std::chrono::time_point< std::chrono::high_resolution_clock > t_start, t_end;
    double t_sum1, t_sum2, duration, result;
    const unsigned runs = 20;
    const double tolerance = 1.e-10;

    const double exact_result = INTEGRAL(b) - INTEGRAL(a);

    std::cout << "# test function: " << FUNCTION_TO_INTEGRATE_NAME << std::endl;
    std::cout << "# interval for integration: <" << a << "," << b
              << ">  discretized using " << bins <<" bins" << std::endl;
    std::cout << "# exact result = " << exact_result << std::endl;
    std::cout << "# function-nr: hard-coded version, function pointer version,"
              << " template version, abstract class version" << std::endl;

    std::cout << FUNCTION_NUMBER << "  ";

    // Hard-coded version
    MEASURE_AND_PRINT_TIME(simpson_hardcoded(a, b, bins))
    std::cout << "  ";

    // Function pointer version version
    MEASURE_AND_PRINT_TIME(simpson_function_ptr(function_to_integrate, a, b, bins))
    std::cout << "  ";

    // Templated function object version (compile-time polymorphism)
    template_function_object tfo;
    MEASURE_AND_PRINT_TIME(simpson_templated_fo(tfo, a, b, bins))
    std::cout << "  ";

    // Abstract base class version (runtime polymorphism)
    derived_function_object dfo;
    MEASURE_AND_PRINT_TIME(simpson_base_class(dfo, a, b, bins))
    std::cout << "\n" << std::endl;

    return 0;
}
