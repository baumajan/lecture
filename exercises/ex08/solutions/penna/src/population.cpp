/**
 * Implementation of the Penna population class.
 * Programming Techniques for Scientific Simulations, ETH Zürich
 */

#include "population.hpp"
#include <vector>
#include <algorithm>
#include <functional>
#include <cstdlib>
#include <iostream>

namespace Penna {

Population::Population( const size_t & nmax, const size_t & n0 )
    : population_( n0 ), nmax_( nmax ) {
}

// We could default the dtor in the hpp, but doing it here keeps the flexibility
// to change it.
Population::~Population() = default;

void Population::simulate( size_t time ) {
    while( time-- ) step();
}

class DeathPredicate {
public:
    DeathPredicate( const double & probability ) : probability_(probability) {};

    bool operator()( Animal const& a ) const {
        return probability_ >= 1. or a.is_dead() or (rand() / double(RAND_MAX)) < probability_;
    };

private:
    const double probability_;
};

void Population::step() {
    // Age all fishes
    // Use C++11 features where they are more elegant than STL algorithms.
    for(auto& animal : population_) {
        animal.grow();
    }

    // Remove dead ones
    population_.remove_if( DeathPredicate( size() / double( nmax_ ) ) );

    // Generate offsprings
    // The range-for-loop stops at the end of the initial range declaration.
    // Using for(... it!= population_.end() ...) would prolong the iteration.
    // CAVE: This only works for containers where push_back doesn't invalidate
    // iterators. We lose generality, but gain readability and performance.
    // See https://en.cppreference.com/w/cpp/container#Iterator_invalidation
    for(auto& animal : population_) {
        if(animal.is_pregnant()) {
            population_.push_back(animal.give_birth());
        }
    }
}

std::size_t Population::size() const {
    return population_.size();
}

Population::const_iterator Population::begin() const {
    return population_.begin();
}

Population::const_iterator Population::end() const {
    return population_.end();
}

// If keeping population_ private
//Population::container_type& Population::get_population() {
//    return population_;
//}

} // end namespace Penna
