#ifndef FISH_POPULATION_HPP
#define FISH_POPULATION_HPP

#include "population.hpp"

namespace Penna {
/**
 * Population of animals with fishing.
 */
class FishPopulation : public Population {
public:
    /**
     * Constructor.
     * @param nmax Maximum population size. Parameter N_{max} in Penna's paper.
     * @param n0 Initial population size.
     * @param frate: Percentage of fish caught.
     * @param fage: Minimum age for removal by fishing.
     */
    FishPopulation(const std::size_t nmax, const std::size_t n0, const double f_rate=0, const std::size_t f_age=0);

    /// Change fishing rate and minium age of fishable fish.
    void change_fishing(const double f_rate, const std::size_t f_age=0);
    
    /// Simulate one time step (year).
    void step();
    
private:
    double f_rate_;
    std::size_t f_age_;
};

} // end namespace Penna

#endif // !defined FISH_POPULATION_HPP
