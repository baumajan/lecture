#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Programming Techniques for Scientific Simulations, ETH Zürich

from .population import *
from .animal import *
from .genome import *
from .fishing import *
