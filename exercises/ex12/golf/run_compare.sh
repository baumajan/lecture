#!/usr/bin/env bash

python3 pygolf.py > result.txt
diff -s result.txt golf_reference.txt

echo "Number of characters:"
wc -c pygolf.py
