/**
 * Implementation of Penna genome class.
 * Programming Techniques for Scientific Simulations, ETH Zürich
 */

#include "genome.hpp"
#include <cstdlib> // drand48()
#include <array>
#include <numeric>

namespace Penna {

// Definition of the static variables.
age_t Genome::mutation_rate_;

void Genome::set_mutation_rate( age_t m ) {
    mutation_rate_ = m;
}

age_t Genome::count_bad( age_t n ) const {
    // Intricate but fast implementation for builtin type sized bitsets:
    // ignore the _first_ (max_age - age) genes and count defective in the rest.
    return (genes_ << (number_of_genes-n-1)).count();
}

/// Pre: mutation_rate <= number_of_genes
void Genome::mutate() {
    // Fisher-Yates shuffle on random access data structure
    // Mutate a random selection of M genes (without replacement)
    std::array<unsigned char,number_of_genes> ages;
    std::iota(std::begin(ages), std::end(ages), 0);
    for(size_t i = 0; i < mutation_rate_; ++i) {
        const size_t pivot = i + drand48() * (number_of_genes - i);
        genes_.flip(ages[pivot]);
        std::swap(ages[pivot], ages[i]);
    }

    // Branchless retry-mask with constant access and good low-density behaviour
    //~ std::bitset<number_of_genes> used(0);
    //~ size_t used_cnt = 0;
    //~ while(used_cnt < mutation_rate_) {
        //~ const size_t pivot = drand48() * (number_of_genes);
        //~ genes_[pivot] = !(used[pivot] ^ genes_[pivot]);
        //~ used_cnt += !used[pivot] * 1;
        //~ used[pivot] = 1;
    //~ }

    // "Incorrect" implementation for future reference
    //~ for(size_t i = 0; i < mutation_rate_; ++i)
        //~ genes_.flip(int(drand48() * number_of_genes));
}

} // namespace Penna
