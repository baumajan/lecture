# Require 3.1 for CMAKE_CXX_STANDARD property
cmake_minimum_required(VERSION 3.1)

project(PennaGenomeAnimalTesting)

set(CMAKE_CXX_STANDARD 11)

# Make it a Release build to add -O3 and -DNDEBUG flags by default
# Comment it out to revert to standard behaviour
set(CMAKE_BUILD_TYPE Release)
# Also add -march-native for Release builds
add_compile_options($<$<CONFIG:RELEASE>:-march=native>)

if(CMAKE_CXX_COMPILER_ID MATCHES "(C|c?)lang")
  add_compile_options(-Weverything)
else()
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

add_library(Penna STATIC genome.cpp animal.cpp)

add_executable(genome-test genome-test.cpp)
add_executable(animal-test animal-test.cpp)

target_link_libraries(genome-test Penna)
target_link_libraries(animal-test Penna)

install(TARGETS genome-test animal-test DESTINATION bin)

install(TARGETS Penna
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib)

install(FILES genome.hpp animal.hpp DESTINATION include)

