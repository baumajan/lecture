# Define our tools and parameters, preferably using the implicit variable names.
CXX = g++
CXXFLAGS = -O3 -std=c++11 -march=native -DNDEBUG
LDFLAGS = -Ltimer
LDLIBS = -ltimer

# Export all variables into the environment. This will come handy further below.
export

# The first target is called 'all' by convention and used to delegate.
.PHONY: all
all: containers.png

# Compile our code and generate an executable binary together with the library.
containers: containers.cpp timer/libtimer.a
	$(CXX) $(CXXFLAGS) -o $@ $< $(LDFLAGS) $(LDLIBS)

# Libraries often know how to build themselves (provide their own build system).
# Exported variables will also be visible in a sub-make.
timer/libtimer.a:
	$(MAKE) -C timer

# Rerun the file if either our program or the input parameter changes.
containers.dat: containers
	./containers | tee containers.dat # "tee" just prints to the terminal and to the file

# Regenerate the plot if the data or plotting configuration changes.
containers.png: containers.dat containers.gnuplot
	gnuplot containers.gnuplot

# Always offer a way to clean up!
.PHONY: clean
clean:
	rm -f containers containers.dat containers.png
	$(MAKE) -C timer clean
