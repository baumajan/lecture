/**
 * Implementation of the Penna population class.
 * Programming Techniques for Scientific Simulations, ETH Zürich
 */

#include "population.hpp"
#include <vector>
#include <algorithm>
#include <functional>
#include <cstdlib>
#include <iostream>

namespace Penna {

Population::Population( const size_t & nmax, const size_t & n0 )
    : nmax_( nmax ), population_( n0 ) {
}

// We could default the dtor in the hpp, but doing it here keeps the flexibility
// to change it.
Population::~Population() = default;

void Population::simulate( size_t time ) {
    while( time-- ) step();
}

class DeathPredicate {
public:
    DeathPredicate( const double & probability ) : probability_(probability) {};

    bool operator()( Animal const& a ) const {
        return probability_ >= 1. or a.is_dead() or (rand() / double(RAND_MAX)) < probability_;
    };

private:
    const double probability_;
};

void Population::step() {
    // Age all fishes
    std::for_each( population_.begin()
                 , population_.end()
                 , std::mem_fn(&Animal::grow)
                 );

    // Remove dead ones
    population_.remove_if( DeathPredicate( size() / double( nmax_ ) ) );

    // Generate offsprings
    std::vector<Animal> parents;
    std::copy_if( population_.begin()
                , population_.end()
                , std::back_inserter(parents)
                , std::mem_fn(&Animal::is_pregnant)
                );
    std::transform( parents.begin()
                  , parents.end()
                  , std::back_inserter(population_)
                  , std::mem_fn(&Animal::give_birth) );
}

std::size_t Population::size() const {
    return population_.size();
}

Population::const_iterator Population::begin() const {
    return population_.begin();
}

Population::const_iterator Population::end() const {
    return population_.end();
}

} // end namespace Penna
