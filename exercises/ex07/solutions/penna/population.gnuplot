set title 'population size over time'
set xlabel 'year'
set ylabel 'population size'

set log xy
set grid

set terminal png
set output 'population.png'

plot 'population.dat' u 1:2 w lp ti ''
