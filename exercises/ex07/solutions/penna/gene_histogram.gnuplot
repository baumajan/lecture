set title 'distribution of bad genes'
set xlabel 'genome position'
set ylabel 'bad fraction'

set yrange [0:1]
set grid

set terminal png
set output 'gene_histogram.png'

plot 'gene_histogram.dat' u 1 w lp ti ''
