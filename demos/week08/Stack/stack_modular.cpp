#include <iostream>
#include <stdexcept>

namespace Stack {

  struct stack {
    double* s; // pointer to allocated stack memory
    double* p; // stack pointer
    int n;     // stack size
  };

  void create(stack& s, int l) {
    s.s = new double[l];
    s.p = s.s;
    s.n = l;
  }

  void destroy(stack& s) {
    s.n = 0;
    s.p = NULL; // or 0 or nullptr (C++11)
    delete[] s.s;
    s.s = NULL; // or 0 or nullptr (C++11)
  }

  void push(stack& s, double v) {
    if ( s.p == s.s + s.n -1 ) {
      throw std::runtime_error("stack overflow");
    }
    *s.p++ = v;
  }

  double pop(stack& s) {
    if ( s.p == s.s ) {
      throw std::runtime_error("stack underflow");
    }
    return *--s.p;
  }

}

int main() {
  try {
    Stack::stack s;
    Stack::create(s,100); // create stack (must be called!)
    Stack::push(s,10.);
    std::cout << Stack::pop(s) << '\n';
    std::cout << Stack::pop(s) << '\n'; // throws error
    Stack::destroy(s); // destroy stack (must be called!)
  }
  catch (std::exception& e) {
    std::cerr << "Exception occurred: " << e.what() << '\n';
    return 1; // return error, i.e. non-zero value!
  }

  return 0; // return 0, i.e. 0 for zero problems!
}
