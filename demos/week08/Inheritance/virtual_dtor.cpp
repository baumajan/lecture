#include <iostream>

class Base {
  public:
    Base() { std::cout << "Base::Base()\n"; }
    ~Base() { std::cout << "Base::~Base()\n"; }
    // virtual ~Base() { std::cout << "Base::~Base()\n"; }
};

class Derived : public Base {
  public:
    Derived() { std::cout << "Derived::Derived()\n"; }
    ~Derived() { std::cout << "Derived::~Derived()\n"; }
};

int main() {
  
  Base* b = new Derived(); // set a Base class pointer to newly created
                           // Derived object

  // ... use b polymorphically ...

  delete b; // Should destruct the Derived object through the Base class
            // pointer.
            // However, if the Base class destructor is not virtual, the
            // Derived class destructor is not called. According to the
            // C++ standard, this leads to undedfined behavior!

}
