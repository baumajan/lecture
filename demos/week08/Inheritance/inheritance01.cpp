#include <iostream>

class Polygon {
  public:
    void set_values(double a, double b) {
      width_  = a;
      height_ = b;
    }
  protected:
    double width_, height_;
};

class Rectangle: public Polygon {
  public:
    double area() {
      return width_*height_;
    }
};

class Triangle: public Polygon {
  public:
    double area() {
      return 0.5*width_*height_;
    }
};
  
int main() {
  Rectangle rec;
  Triangle  tri;
  rec.set_values(4., 5.);
  tri.set_values(4., 5.);
  std::cout << "Rectangle area: " << rec.area() << '\n';
  std::cout << "Triangle  area: " << tri.area() << '\n';
}
