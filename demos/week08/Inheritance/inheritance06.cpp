#include <iostream>

class Base {
  public:
    void func() { std::cout << "Base::func()" << std::endl; }
};

class Derived : public Base {
  public:
    // Redefining an inherited non-firtual function, WTF!
    void func() { std::cout << "Derived::func()" << std::endl; }
};

void polyfunc(Base& a) {
  a.func();
}

int main() {
  Base b;
  Derived d;
  b.func();
  d.func();
  polyfunc(b);
  polyfunc(d);
}
