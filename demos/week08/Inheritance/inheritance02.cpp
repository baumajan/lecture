#include <iostream>

class Mother {
  public:
    Mother() { std::cout << "Mother::Mother()\n"; }         // default ctor
    Mother(int a) { std::cout << "Mother::Mother(int)\n"; } // ctor
    ~Mother() { std::cout << "Mother::~Mother()\n"; }       // dtor
};

class Daughter : public Mother {
  public:
    Daughter(int a) { std::cout << "Daughter::Daughter(int)\n"; } // ctor
    ~Daughter() { std::cout << "Daughter::~Daughter()\n"; }       // dtor
};

class Son : public Mother {
  public:
    Son(int a) : Mother(a) { std::cout << "Son::Son(int)\n"; } // ctor
    ~Son() { std::cout << "Son::~Son()\n"; }                   // dtor
};

int main() {
  Mother peggy;
  Daughter kelly(0);
  Son bud(0);
}
