#include <cmath>
#include <iostream>

// trapezoidal rule
double integrate(double (*f)(double), double a, double b, unsigned int N) {
  double dx = (b - a )/N;
  double xi = a;
  double I = 0.5*f(xi);
  for (unsigned int i = 1; i < N; ++i) {
    xi += dx;
    I += f(xi);
  }
  I += 0.5*f(b);
  return I*dx;
}

double func(double x) {
  return x*std::sin(x);
}

int main() {
  std::cout <<"I[x*sin(x)] = " << integrate(func, 0., 1., 100) << '\n';
}
