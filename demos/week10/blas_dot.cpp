#include <cstdlib>
#include <iostream>

extern "C" 
double ddot_(int& N, double* X, int& INCX, double* Y, int& INCY);

int main() {

  // vectors
  int N = 16384;
  double* a = new double[N];
  double* b = new double[N];

  // initialize vectors with random numbers
  srand48(42);
  for (int i=0; i<N; ++i) {
    a[i] = drand48();
    b[i] = drand48();
  }

  // compute scalar product
  int inc = 1;
  double d = ddot_(N, &a[0], inc, &b[0], inc);
  std::cout << "a . b = " << d << std::endl;

  // deallocate
  delete[] a;
  delete[] b;

}
