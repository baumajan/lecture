// compile with sse4 enabled
// E.g., gnu/clang: g++ -msse4 intrinsic.cpp
#include <iostream>
#include <nmmintrin.h> // intrinsics header

int bitcount(unsigned int x) {
  int cnt = 0;
  for (int i=0 ; i<32 ;++i) {
    if ( x & 1 ) ++cnt; // check lower bit
    x = (x >> 1);       // shift bits by one to the right
  }
  return cnt;
}

int main() {
  unsigned int x;
  std::cout << "Enter x: ";
  std::cin >> x;
  int cnt = _mm_popcnt_u32(x);
  std::cout << x << " has " << cnt         << " bits set [bitcount]\n";
  std::cout << x << " has " << bitcount(x) << " bits set [_mm_popcnt_u32]\n";
}
