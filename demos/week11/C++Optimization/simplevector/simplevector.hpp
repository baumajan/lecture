#include <algorithm> // for std::swap
#include <cassert>

template <typename T>
class simplevector {
  public:
    typedef T value_type;
    typedef T& reference;
    typedef unsigned int size_type;

    // ctor
    explicit simplevector(size_type s=0) : p_(new value_type[s]), sz_(s) {}
    // copy ctor
    simplevector(const simplevector& v) : p_(new value_type[v.size()])
                                        , sz_(v.size()) {
      for (size_type i=0; i<size(); ++i) {
        p_[i] = v.p_[i];
      }
    }
    // dtor
    ~simplevector() { delete[] p_; }
    // swap
    void swap(simplevector& v) {
      std::swap(p_,v.p_);
      std::swap(sz_,v.sz_);
    }
    // copy assignment
    simplevector& operator=(simplevector v) {
      swap(v);
      return *this;
    }
    // compound plus assignment 
    simplevector& operator+=(const simplevector& v) {
      assert(size() == v.size());
      for (size_type i=0; i<size(); ++i) {
        p_[i] += v.p_[i];
      }
      return *this;
    }
    // size
    size_type size() const { return sz_; }
    // subscript operator
    value_type operator[](size_type i) const { return p_[i]; }
    reference operator[](size_type i) { return p_[i]; }

  private:
    value_type* p_;
    size_type sz_;
};

// binary plus operator (non-member / free function)
template <typename T>
simplevector<T> operator+(const simplevector<T>& x,
                          const simplevector<T>& y) {
  simplevector<T> result = x;
  result += y;
  return result;
}
