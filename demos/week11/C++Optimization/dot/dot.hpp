#include "tinyvector.hpp"

// The metaprogram
template<int I>
struct meta_dot {
  template<typename T,int N>
  static T f(TinyVector<T,N>& a, TinyVector<T,N>& b) {
    return a[I]*b[I] + meta_dot<I-1>::f(a,b);
  }
};

// The end of the recursion
template<>
struct meta_dot<0> {
  template<typename T,int N>
  static T f(TinyVector<T,N>& a, TinyVector<T,N>& b) {
    return a[0]*b[0];
  }
};

// The dot() function invokes meta_dot -- the metaprogram
template<typename T,int N>
inline float dot(TinyVector<T,N>& a, TinyVector<T,N>& b) {
  return meta_dot<N-1>::f(a,b);
}
