import math

print("Table with default format:")
print("i", "PI**i", "PI**(2*i)", "PI**(3*i)")
for i in range(-5, +5+1):
    print(i, math.pi**(i), math.pi**(2*i), math.pi**(3*i))
print(80*"#")

print("Table with some formatting:")
print("{:5s} {:15s} {:15s} {:15s}".format("i", "PI**i", "PI**(2*i)",
                                          "PI**(3*i)"))
for i in range(-5, +5+1):
    print("{:5d} {:15g} {:15g} {:15g}".format(i, math.pi**(  i),
                                                 math.pi**(2*i),
                                                 math.pi**(3*i)))
print(80*"#")

print("Table with more formatting:")
print("{:5s} {:15s} {:15s} {:15s}".format("i", "PI**i", "PI**(2*i)",
                                          "PI**(3*i)"))
for i in range(-5,+5+1):
    print("{:5d} {:15.8e} {:15.8e} {:15.8e}".format(i, math.pi**(  i),
                                                      -math.pi**(2*i),
                                                      -math.pi**(3*i)))
print(80*"#")
