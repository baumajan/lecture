x = dict(a=1, b=2, c='three')
x = {'a': 1, 'b': 2, 'c': 'three'}

# access via []
x['a'] == 1

# creating new entries
# any hashable type can be a key
x[1] = 4

# accessing keys, values or both
# order is not preserved
x.keys() # ['a', 'c', 1, 'b']
x.values() # [1, 'three', 4, 2]
x.items() # [('a', 1), (c, 'three'), (1, 4), ('b', 2)] 
