def apply(func, x):
    return func(x)

x = apply(lambda z: z**2, 2.)
print(x)
