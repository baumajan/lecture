#include <algorithm>
#include <iostream>
#include <list>
#include <numeric>
#include <vector>

template <typename T>
void print_func(T const& elem) {
  std::cout << elem << " ";
}

struct Print {
  template <typename T>
  void operator()(T const& elem) { std::cout << elem << " "; }
};

struct Sum {
  Sum() : sum(0) {}
  void operator()(int i) { sum += i; }
  int sum;
};

int main() {

  int array[] = {42, 3, 7, 666, 13, 8}; // built-in array
  std::vector<int> vec(array, array+5); // vector<int> with
  std::list<int> lst(vec.begin(), vec.end()); // list<int> with

  // find a value in containers
  int val2find = 42;

  // find in bluit-in array
  std::cout << "Found value in bluit-in array: "
            << *std::find(array, array+5, val2find) << std::endl;

  // find in vector
  std::cout << "Found value in vector: "
            << *std::find(vec.begin(), vec.end(), val2find) << std::endl;

  // find in list
  std::list<int>::iterator fnd = std::find(lst.begin(), lst.end(), val2find);
  if ( fnd != lst.end() )
    std::cout << "Found value: " << *fnd << std::endl;
  else
    std::cout << "Not found" << std::endl;

  // equal
  lst.push_back(157); // add 49 to list, are vec & lst equal, i.e., same elems?
  if ( std::equal(vec.begin(), vec.end(), lst.begin()) )
    std::cout << "vec and lst are the same!" << std::endl;
  else
     std::cout << "vec and lst are NOT the same!" << std::endl;

  // for_each to print the list lst
  // with function object
  std::for_each(lst.begin(), lst.end(), Print());
  std::cout << std::endl;
  // with a pointer to a function
  std::for_each(lst.begin(), lst.end(), &print_func<int>);
  std::cout << std::endl;
  // or with a "named" c++11 lambda!
  auto print_lmbda = [](const int& n) { std::cout << n << " "; };
  std::for_each(lst.begin(), lst.end(), print_lmbda);
  std::cout << std::endl;
  // or simply with an "anonymous" c++11 lambda!
  std::for_each(lst.begin(), lst.end(),
                [](const int& n) { std::cout << n << " "; });

  // for_each to compute sum of container
  Sum sum = std::for_each(vec.begin(), vec.end(), Sum());
  std::cout << "Sum of vec is " << sum.sum << std::endl;

  // or use accumulate defined (in <numeric>!)
  std::cout << "Sum of lst is "
            << std::accumulate(lst.begin(), lst.end(), 0)
            << std::endl;

}
