#include <algorithm>
#include <iostream>
#include <list>
#include <string>

bool favorite_fruits(const std::string& name) {
  return (name == "Apple" || name == "Orange");
}

int main() {

  // first list of fruits: adding elements with push_back
  std::list<std::string> fruits1;
  fruits1.push_back("Apple");
  fruits1.push_back("Orange");
  fruits1.push_back("Banana");
  fruits1.push_back("Peach");

  // second list of fruits: using initializer list C++11
  std::list<std::string> fruits2 = {"Grape", "Lemon", "Orange", "Strawberry"};

  // splice fruits2 into fruits1
  fruits1.splice(fruits1.end(), fruits2);

  // print the lists
  std::cout << "fruits1: ";
  for (std::list<std::string>::iterator it = fruits1.begin();
       it != fruits1.end(); ++it) {
    std::cout << *it << " ";
  }
  std::cout << '\n';
  std::cout << "fruits2: ";
  for (std::list<std::string>::iterator it = fruits2.begin();
      it != fruits2.end(); ++it) {
    std::cout << *it << " ";
  }
  std::cout << '\n';

  // search fruits1 for Apple
  std::list<std::string>::const_iterator
  found = std::find(fruits1.begin(), fruits1.end(), "Apple");
  if ( found == fruits1.end() ) { // end means invalid iterator, i.e. not found!
    std::cout << "No apple in the list\n";
  } else {
    std::cout << "Found it: " << *found << "\n";
  }

  found = std::find_if(fruits1.begin(), fruits1.end(), favorite_fruits);
  if ( found == fruits1.end() ) { // end means invalid iterator, i.e. not found!
    std::cout << "No favorite fruit in the list\n";
  } else {
    std::cout << "Found a favorite fruit!\n";
  }

}
