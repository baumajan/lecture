#include "slist.hpp"
#include <iostream>

int main() {

  slist<int> l;

  // add some elements to list
  l.addElement(0);
  l.addElement(1);
  l.addElement(2);
  l.addElement(3);
  l.addElement(4);

  // print the list
  std::cout << "l = ";
  for (node<int>* p=l.first; p!=nullptr; p=p->next) {
    std::cout << p->value << " ";
  }
  std::cout << '\n';

  // print the list using our iterator
  std::cout << "l = ";
  for (slist<int>::iterator p=l.begin(); p!=l.end(); ++p) {
    std::cout << *p << " ";
  }
  std::cout << '\n';

  // even more convenient: print list using range-based for
  std::cout << "l = ";
  for (auto const& elem : l) {
    std::cout << elem << " ";
  }
  std::cout << '\n';

}
