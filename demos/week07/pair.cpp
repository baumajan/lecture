#include <iostream>
#include <utility>

std::pair<std::string,int> some_pair(std::string w, int i) {
  return std::make_pair(w,i);
  // or simply in C++11 with list initialization
  // return {w,i};
}

template <typename T1,typename T2>
void print_pair(std::pair<T1,T2> pair) {
  std::cout << "("  << pair.first 
            << ", " << pair.second 
            << ")\n";
}

int main() {

  std::pair<int,double> a(1,3.);
  std::cout << "a.first  = " << a.first  << " , "
            << "a.second = " << a.second << '\n';

  std::pair<std::string,int> b;
  b = some_pair("something", 7);
  print_pair(b);

  // C++11 list initialization of pair
  std::pair<std::string,std::string> c = {"first", "second"};
  print_pair(c);

}
