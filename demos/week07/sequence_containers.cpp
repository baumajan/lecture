#include <iostream>
#include <list>
#include <vector>

template <typename C>
void print_container(C& c) {
  for (auto const& elem : c) {
    std::cout << elem << " ";
  }
}

template <typename C>
void setDummy_container(C& c) {
  int i = 0;
  for (auto& elem : c) {
    elem = i++;
  }
}

int main() {

  using elem_type = double;
  using C = std::list<elem_type>;
//  using C = std::vector<elem_type>;


  // create container with 7 instances of elem_type
  C c(7);

  // set some dummy values
  std::cout << "Set some values:\n";
  setDummy_container(c);
  std::cout << "c = ";
  print_container(c);
  std::cout << '\n';

  // insert before 3 element
  std::cout << "Insert before 3 element\n";
  auto it = c.begin(); // get iterator to beginning
  std::advance(it, 3); // advance iterator by 3 elements
  c.insert(it, 3.5);   // insert value
  std::cout << "c = ";
  print_container(c);
  std::cout << '\n';

  // erase element at position 3
  std::cout << "Erase element at position 3:\n";
  it = c.begin(); // set iterator to beginning
  std::advance(it, 3); // advance iterator by 3 elements
  c.erase(it);   // insert value
  std::cout << "c = ";
  print_container(c);
  std::cout << '\n';

  // clear
  std::cout << "Clear:\n";
  c.clear();
  std::cout << "c = ";
  print_container(c);
  std::cout << '\n';

}
