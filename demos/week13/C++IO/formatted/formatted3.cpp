#include <iostream>
#include <iomanip>

int main() {

  // precision
  std::cout << std::setprecision(2); // sticky!
  std::cout << "|" << 123.456789 << "|" << std::endl;
  std::cout << std::setprecision(2); // sticky!
  std::cout << "|" << 123.       << "|" << std::endl;
  std::cout << std::setprecision(5); // sticky!
  std::cout << "|" << 123.456789 << "|" << std::endl;
  std::cout << std::setprecision(15); // sticky!
  std::cout << "|" << 123.456789 << "|" << std::endl;
  std::cout << std::setprecision(6); // reset to default

  // default formatting (defaultfloat c++11!)
  std::cout << std::defaultfloat; // sticky!
  std::cout << "|" << 1234567.89 << "|" << std::endl;

  // fixed-point formatting
  std::cout << std::fixed; // sticky!
  std::cout << "|" << 1234567.89 << "|" << std::endl;
 
  // scientific formatting
  std::cout << std::scientific;   
  std::cout << "|" << 1234567.89 << "|" << std::endl;
 
}

