#include <iostream>

int main() {

  int ival;
  std::cout << "Enter integer ival = ";
  if (std::cin  >> ival) { // check if input valid
    std::cout << "You entered ival = " << ival << '\n';
  } else { // invalid input!
    std::cerr << "Bad input!" << '\n';
  }

}
