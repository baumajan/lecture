#include <hdf5.h> // hdf5 header
#include <iostream>

int main() {

  hid_t file_id;      // file    identifier
  hid_t dataset_id;   // dataset identifier
  herr_t status;      // error code

  // initialize data with some values
  int dset_data[4][6];
  for(int i = 0; i < 4; ++i) {
    for (int j = 0; j < 6; ++j) {
      dset_data[i][j] = 6*i + j;
    }
  }

  // open existing file using default (creation & access) properties
  file_id = H5Fopen("dataset.h5",  // file name
                    H5F_ACC_RDWR, // file access flags
                                  // (read & write here)
                    H5P_DEFAULT); // file access properties list
  // check if open succeeded (file_id negative means failure!)
  if ( file_id < 0 ) {
    std::cout << "Error reading dataset.h5!\n";
    return -1;
  }

  // open existing dataset
  dataset_id = H5Dopen2(file_id,      // location identifier
                        "/dset",      // dataset name
                        H5P_DEFAULT); // dataset access property list

  // write the dataset
  status = H5Dwrite(dataset_id,     // dataset identifier
                    H5T_NATIVE_INT, // memory datatype
                    H5S_ALL,        // memory dataspace (H5S_ALL means all)
                    H5S_ALL,        // dataset's dataspace in the file
                    H5P_DEFAULT,    // transfer property list
                    dset_data);    // buffer with data to be written

  // close the dataset
  status = H5Dclose(dataset_id);

  // close the file
  status = H5Fclose(file_id);

}
