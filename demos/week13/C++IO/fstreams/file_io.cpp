#include <iostream>
#include <fstream>
#include <string>

int main() {

  std::string fname("test.txt"); // file name

  // write to  file //
  // construct & connect to file
  std::ofstream fout(fname); // default mode:   fstream::out
                             //               | fstream::trunc
  // check if it succeeded
  if (!fout) {
    std::cerr << "Error: open file for output failed!" << std::endl;
    return -1;
  }
  // write some stuff
  fout << "Kawasaki\n";
  fout << "Yamaha\n";
  fout << "Suzuki\n";
  fout << "Honda\n";
  // close the file
  fout.close(); 

  // read from file //
  // construct & connect to file
  std::ifstream fin(fname); // default mode: fstream::in
  // check if it succeeded
  if (!fin) {
    std::cerr << "Error: open file for input failed!\n";
    return -1;
  }
  std::string word;
  while (fin >> word) {  // read until end-of-file (a.k.a. EoF)
     std::cout << word << '\n';
  }
  fin.close();

}
