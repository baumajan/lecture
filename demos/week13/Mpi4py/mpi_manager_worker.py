import sys
from mpi4py import MPI # MPI_Init and MPI_Finalize automatically called
import numpy as np
from mpi_task import *

# To run this on Euler, load the following modules
#
#   module load open_mpi python
#
# and submit it to the batch system, e.g.
#
#   bsub -n 10 mpirun python mpi_manager_worker.py
#

# some parameters
MANAGER = 0        # rank of manager
TAG_TASK       = 1 # task       message tag
TAG_TASK_DONE  = 2 # tasks done message tag
TAG_DONE       = 3 # done       message tag

def manager(comm,tasks):

    """
    manager
    """

    # get number of processes in communicator comm
    size = comm.Get_size()

    # define MPI status object (for getting source, tag, ...)
    status = MPI.Status()

    # setup tasks & per process task counter
    Ntasks = len(tasks)
    TasksDoneByWorker = [0]*size

    # send a first task to each worker
    SentTasks = 0 # sent task counter
    for idest in range(size):
        if ( idest == MANAGER ): # no task to manager!
            continue
        if ( SentTasks < Ntasks ):
            comm.send(tasks.pop(),dest=idest,tag=TAG_TASK)
            SentTasks += 1 # increment sent task counter
        else: # treat case with more processes than tasks!
            comm.send(0,dest=idest,tag=TAG_DONE)

    # send & receive tasks to/from workers
    tasks_done = [] # initialize empty done task list
    while ( len(tasks_done) < Ntasks ):
        # receive done task from worker
        task_done = comm.recv(source=MPI.ANY_SOURCE \
                             ,tag=TAG_TASK_DONE     \
                             ,status=status)
        tasks_done += [task_done] # append to done tasks list
        source = status.Get_source() # get rank of worker who did the task
        TasksDoneByWorker[source] += 1 # increment tasks done counter of this
                                       # worker
        # send the worker a new task (if there is)
        if ( tasks ):
            comm.send(tasks.pop(),dest=source,tag=TAG_TASK)

        else: # inform worker that there are no more tasks
            comm.send(0,dest=source,tag=TAG_DONE)

    # return
    return tasks_done,TasksDoneByWorker

def worker(comm):

    """
    worker
    """

    # get number of processes & rank in communicator comm
    size    = comm.Get_size()
    my_rank = comm.Get_rank()

    # define MPI status object (for getting source, tag, ...)
    status = MPI.Status()

    # receive tasks
    while ( True ):
        task = comm.recv(source=MANAGER,tag=MPI.ANY_TAG,status=status)
        tag = status.Get_tag()
        if   ( tag == TAG_TASK ): # do the received task
            task.do_task()
            comm.send(task,dest=MANAGER,tag=TAG_TASK_DONE)

        elif ( tag == TAG_DONE ): # no more tasks, holidays!
            break

        else: # should in principle not happen!
            msg = "Error: unknown tag = {} ".format(tag)      \
                + "received by process {:5d}".format(my_rank)
            print(msg)
            break

    # return
    return None

if __name__ == "__main__":

    # get COMMON WORLD communicator, size & rank
    comm    = MPI.COMM_WORLD
    size    = comm.Get_size()
    my_rank = comm.Get_rank()

    # report on MPI environment
    if ( my_rank == MANAGER ):
        print("MPI initialized with {:5d} processes".format(size))

    # check that at least one worker! (managers just delegate!!!)
    if ( size == 1 ):
        msg = "Error: at least one worker needed (at least 2 MPI processes)!"
        sys.exit(msg)

    # separate manager & worker(s)
    if ( my_rank == MANAGER ): # manager
        # create a random matrix & vector
        Nrows    = 1000
        Ncolumns = 1000
        A = np.random.rand(Nrows,Ncolumns)
        b = np.random.rand(Nrows)
        # create tasks & tasks_done lists
        tasks = []
        for irow in range(Nrows):
          itask = task(irow,A[irow,:],b)
          tasks.append(itask)

        # manager
        [tasks_done,TasksDoneByWorker] = manager(comm,tasks)

        # collect done tasks and compare
        Ab  = np.zeros(Nrows)
        while ( len(tasks_done) > 0 ):
          task = tasks_done.pop()
          Ab[task.row] = task.ab
        print('Info: Result error = %e' % np.linalg.norm(Ab - np.dot(A,b)))

    else: # worker
        worker(comm)

    # inform that done
    if ( my_rank == MANAGER ):
        for i in range(size):
            if ( i == MANAGER ):
                continue
            msg  = "Process {:5d} ".format(i)
            msg += "has done {:10d} tasks".format(TasksDoneByWorker[i])
            print(msg)
        print("Done.")
