import sys
import h5py

# open file
try:
    f = h5py.File("file.h5", "r")
except OSError:
    sys.exit("Error: file.h5 not found! Run h5_write.py first!")

# print file content
f.visititems(lambda name, h5obj: print(f"/{name}:", h5obj))

# read dataset dset
dset = f["/dset"][:]
print("dset = ", dset)

# read & print dset2 dataset
dset2 = f["/somegroup/dset2"][()]
print("dset2 =", dset2)

# close the file
f.close()
